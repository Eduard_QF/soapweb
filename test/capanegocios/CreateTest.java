/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package capanegocios;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import orm.Actividad;
import orm.Apoderado;
import orm.Asignatura;
import orm.Curso;
import orm.Estudiante;
import orm.Institucion;
import orm.Persona;
import orm.Planificacion;
import orm.Profesor;

/**
 *
 * @author Eduard QF
 */
public class CreateTest {
    
    public CreateTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of createInstitucion method, of class Create.
     */
    @Test
    public void testCreateInstitucion_String() throws Exception {
        System.out.println("createInstitucion");
        String nombreInstitucion = "ufro";
        boolean expResult = false;
        boolean result = Create.createInstitucion(nombreInstitucion);
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of createInstitucion method, of class Create.
     */
    @Test
    public void testCreateInstitucion_String_CursoArr() throws Exception {
        System.out.println("createInstitucion");
        String nombreInstitucion = "";
        Curso[] cursos = null;
        boolean expResult = false;
        boolean result = Create.createInstitucion(nombreInstitucion, cursos);
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of createCurso method, of class Create.
     */
    @Test
    public void testCreateCurso_String_Institucion() throws Exception {
        System.out.println("createCurso");
        String idCurso = "";
        Institucion institucion = null;
        boolean expResult = false;
        boolean result = Create.createCurso(idCurso);
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of createCurso method, of class Create.
     */
    @Test
    public void testCreateCurso_3args_1() throws Exception {
        System.out.println("createCurso");
        String idCurso = "";
        Institucion institucion = null;
        Estudiante[] alumno = null;
        boolean expResult = false;
        boolean result = Create.createCurso(idCurso, institucion, alumno);
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of createCurso method, of class Create.
     */
    @Test
    public void testCreateCurso_3args_2() throws Exception {
        System.out.println("createCurso");
        String idCurso = "";
        Institucion institucion = null;
        Asignatura[] asign = null;
        boolean expResult = false;
        boolean result = Create.createCurso(idCurso, institucion, asign);
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of createCurso method, of class Create.
     */
    @Test
    public void testCreateCurso_4args() throws Exception {
        System.out.println("createCurso");
        String idCurso = "";
        Institucion institucion = null;
        Estudiante[] alumno = null;
        Asignatura[] asign = null;
        boolean expResult = false;
        boolean result = Create.createCurso(idCurso, institucion, alumno, asign);
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of createAsignatura method, of class Create.
     */
    @Test
    public void testCreateAsignatura() throws Exception {
        System.out.println("createAsignatura");
        String nombre = "";
        Curso cur = null;
        Profesor profesor = null;
        boolean expResult = false;
        boolean result = Create.createAsignatura(nombre, cur, profesor);
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of createEstudiante method, of class Create.
     */
    @Test
    public void testCreateEstudiante_4args() throws Exception {
        System.out.println("createEstudiante");
        String nombre = "";
        String rut = "";
        Curso cur = null;
        Apoderado ap = null;
        boolean expResult = false;
        boolean result = Create.createEstudiante(nombre, rut, cur, ap);
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of createEstudiante method, of class Create.
     */
    @Test
    public void testCreateEstudiante_5args() throws Exception {
        System.out.println("createEstudiante");
        String nombre = "";
        String rut = "";
        Curso cur = null;
        String nombreAP = "";
        String rutAP = "";
        boolean expResult = false;
        boolean result = Create.createEstudiante(nombre, rut, cur, nombreAP, rutAP);
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of createApoderado method, of class Create.
     */
    @Test
    public void testCreateApoderado() throws Exception {
        System.out.println("createApoderado");
        String nombre = "";
        String rut = "";
        Estudiante alumno = null;
        boolean expResult = false;
        boolean result = Create.createApoderado(nombre, rut, alumno);
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of createProfesor method, of class Create.
     */
    @Test
    public void testCreateProfesor_String_String() throws Exception {
        System.out.println("createProfesor");
        String nombre = "";
        String rut = "";
        boolean expResult = false;
        boolean result = Create.createProfesor(nombre, rut);
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of createProfesor method, of class Create.
     */
    @Test
    public void testCreateProfesor_3args_1() throws Exception {
        System.out.println("createProfesor");
        String nombre = "";
        String rut = "";
        Asignatura[] asigns = null;
        boolean expResult = false;
        boolean result = Create.createProfesor(nombre, rut, asigns);
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of createProfesor method, of class Create.
     */
    @Test
    public void testCreateProfesor_3args_2() throws Exception {
        System.out.println("createProfesor");
        String nombre = "";
        String rut = "";
        Asignatura asigns = null;
        boolean expResult = false;
        boolean result = Create.createProfesor(nombre, rut, asigns);
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of createPlanificacion method, of class Create.
     */
    @Test
    public void testCreatePlanificacion_Asignatura() throws Exception {
        System.out.println("createPlanificacion");
        Asignatura asign = null;
        boolean expResult = false;
        boolean result = Create.createPlanificacion(asign);
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of createPlanificacion method, of class Create.
     */
    @Test
    public void testCreatePlanificacion_Asignatura_ActividadArr() throws Exception {
        System.out.println("createPlanificacion");
        Asignatura asign = null;
        Actividad[] actividades = null;
        boolean expResult = false;
        boolean result = Create.createPlanificacion(asign, actividades);
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of createActividad method, of class Create.
     */
    @Test
    public void testCreateActividad() throws Exception {
        System.out.println("createActividad");
        String detalle = "";
        String fecha = "";
        Planificacion plan = null;
        boolean expResult = false;
        boolean result = Create.createActividad(detalle, fecha, plan);
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of createAsistencia method, of class Create.
     */
    @Test
    public void testCreateAsistencia() throws Exception {
        System.out.println("createAsistencia");
        boolean presencia = false;
        String fecha = "";
        Estudiante alumno = null;
        boolean expResult = false;
        boolean result = Create.createAsistencia(presencia, fecha, alumno);
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of createAnotacion method, of class Create.
     */
    @Test
    public void testCreateAnotacion() throws Exception {
        System.out.println("createAnotacion");
        String detalle = "";
        String fecha = "";
        boolean tipoAnotacion = false;
        Estudiante alumno = null;
        Profesor docente = null;
        boolean expResult = false;
        boolean result = Create.createAnotacion(detalle, fecha, tipoAnotacion, alumno, docente);
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of createNota method, of class Create.
     */
    @Test
    public void testCreateNota() throws Exception {
        System.out.println("createNota");
        float nota = 0.0F;
        Actividad act = null;
        Estudiante alumno = null;
        boolean expResult = false;
        boolean result = Create.createNota(nota, act, alumno);
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of createPersona method, of class Create.
     */
    @Test
    public void testCreatePersona_String_String() throws Exception {
        System.out.println("createPersona");
        String nombre = "";
        String rut = "";
        Persona expResult = null;
        Persona result = Create.createPersona(nombre, rut);
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of createPersona method, of class Create.
     */
    @Test
    public void testCreatePersona_3args_1() throws Exception {
        System.out.println("createPersona");
        String nombre = "";
        String rut = "";
        Apoderado ap = null;
        Persona expResult = null;
        Persona result = Create.createPersona(nombre, rut, ap);
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of createPersona method, of class Create.
     */
    @Test
    public void testCreatePersona_3args_2() throws Exception {
        System.out.println("createPersona");
        String nombre = "";
        String rut = "";
        Estudiante es = null;
        Persona expResult = null;
        Persona result = Create.createPersona(nombre, rut, es);
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of createPersona method, of class Create.
     */
    @Test
    public void testCreatePersona_3args_3() throws Exception {
        System.out.println("createPersona");
        String nombre = "";
        String rut = "";
        Profesor docente = null;
        Persona expResult = null;
        Persona result = Create.createPersona(nombre, rut, docente);
        assertEquals(expResult, result);
        
        
    }
    

}
