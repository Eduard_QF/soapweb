/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package capanegocios;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import orm.Actividad;
import orm.Asignatura;
import orm.Curso;
import orm.Estudiante;
import orm.Institucion;
import orm.Planificacion;

/**
 *
 * @author Eduard QF
 */
public class DeleteTest {
    
    public DeleteTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of deleteInstitucion method, of class Delete.
     */
    @Test
    public void testDeleteInstitucion() throws Exception {
        System.out.println("deleteInstitucion");
        boolean expResult = false;
        boolean result = Delete.deleteInstitucion();
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of deleteCurso method, of class Delete.
     */
    @Test
    public void testDeleteCurso() throws Exception {
        System.out.println("deleteCurso");
        String idCurso = "1 A"; 
        Institucion institucion = null;
        boolean expResult = false;
        boolean result = Delete.deleteCurso(idCurso, institucion);
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of deleteAsignatura method, of class Delete.
     */
    @Test
    public void testDeleteAsignatura() throws Exception {
        System.out.println("deleteAsignatura");
        String nombre = "";
        Curso cur = null;
        boolean expResult = false;
        boolean result = Delete.deleteAsignatura(nombre, cur);
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of deleteEstudiante method, of class Delete.
     */
    @Test
    public void testDeleteEstudiante() throws Exception {
        System.out.println("deleteEstudiante");
        String nombre = "";
        String rut = "";
        boolean expResult = false;
        boolean result = Delete.deleteEstudiante(nombre, rut);
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of deleteApoderado method, of class Delete.
     */
    @Test
    public void testDeleteApoderado() throws Exception {
        System.out.println("deleteApoderado");
        String nombre = "";
        String rut = "";
        boolean expResult = false;
        boolean result = Delete.deleteApoderado(nombre, rut);
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of deleteProfesor method, of class Delete.
     */
    @Test
    public void testDeleteProfesor() throws Exception {
        System.out.println("deleteProfesor");
        String nombre = "";
        String rut = "";
        boolean expResult = false;
        boolean result = Delete.deleteProfesor(nombre, rut);
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of deletePlanificacion method, of class Delete.
     */
    @Test
    public void testDeletePlanificacion() throws Exception {
        System.out.println("deletePlanificacion");
        Asignatura asign = null;
        boolean expResult = false;
        boolean result = Delete.deletePlanificacion(asign);
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of deleteActividad method, of class Delete.
     */
    @Test
    public void testDeleteActividad() throws Exception {
        System.out.println("deleteActividad");
        String fecha = "";
        Planificacion plan = null;
        boolean expResult = false;
        boolean result = Delete.deleteActividad(fecha, plan);
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of deleteAsistencia method, of class Delete.
     */
    @Test
    public void testDeleteAsistencia() throws Exception {
        System.out.println("deleteAsistencia");
        String fecha = "";
        Estudiante alumno = null;
        boolean expResult = false;
        boolean result = Delete.deleteAsistencia(fecha, alumno);
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of deleteAnotacion method, of class Delete.
     */
    @Test
    public void testDeleteAnotacion() throws Exception {
        System.out.println("deleteAnotacion");
        String fecha = "";
        Estudiante alumno = null;
        boolean expResult = false;
        boolean result = Delete.deleteAnotacion(fecha, alumno);
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of deleteNota method, of class Delete.
     */
    @Test
    public void testDeleteNota() throws Exception {
        System.out.println("deleteNota");
        Actividad act = null;
        Estudiante alumno = null;
        boolean expResult = false;
        boolean result = Delete.deleteNota(act, alumno);
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of deletePersona method, of class Delete.
     */
    @Test
    public void testDeletePersona() throws Exception {
        System.out.println("deletePersona");
        String nombre = "";
        String rut = "";
        boolean expResult = false;
        boolean result = Delete.deletePersona(nombre, rut);
        assertEquals(expResult, result);
        
        
    }
    
}
