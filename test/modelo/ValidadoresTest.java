/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import orm.Estudiante;
import orm.Nota;

/**
 *
 * @author Eduard QF
 */
public class ValidadoresTest {

    public ValidadoresTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of validarRut method, of class Validadores.
     */
    @Test
    public void testValidarRut() {
        System.out.println("validarRut");
        String cadena = "";
        boolean expResult = false;
        boolean result = Validadores.validarRut(cadena);
        assertEquals(expResult, result);

    }

    /**
     * Test of validarFormatoRut method, of class Validadores.
     */
    @Test
    public void testValidarFormatoRut() {
        System.out.println("validarFormatoRut");
        String r = "";
        boolean expResult = false;
        boolean result = Validadores.validarFormatoRut(r);
        assertEquals(expResult, result);

    }

    /**
     * Test of vFormatnumPos method, of class Validadores.
     */
    @Test
    public void testVFormatnumPos() {
        System.out.println("vFormatnumPos");
        String text = "";
        boolean expResult = false;
        boolean result = Validadores.vFormatnumPos(text);
        assertEquals(expResult, result);

    }

    /**
     * Test of porsentaje method, of class Validadores.
     */
    @Test
    public void testPorsentaje() {
        System.out.println("porsentaje");
        float porsientoAsistencia = 0.0F;
        int pAsis = 0;
        boolean expResult = false;
        boolean result = Validadores.porsentaje(porsientoAsistencia, pAsis);
        assertEquals(expResult, result);

    }

    /**
     * Test of validarFormatoNombre method, of class Validadores.
     */
    @Test
    public void testValidarFormatoNombre() {
        System.out.println("validarFormatoNombre");
        String text = "Eduardo";
        boolean expResult = true;
        boolean result = Validadores.validarFormatoNombre(text);
        assertEquals(expResult, result);

    }

    /**
     * Test of reprobado method, of class Validadores.
     */
    @Test
    public void testReprobado() {
        System.out.println("reprobado");
        Estudiante alumn = new Estudiante();
        boolean expResult = false;
        boolean result = Validadores.reprobado(alumn);
        assertEquals(expResult, result);

    }

    @Test
    public void testValidadorFormatoCurso() {
        boolean expResult = true;
        boolean result = Validadores.validarFormatCurso("1 A");
        assertEquals(expResult, result);
    }

}
