package datos;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import org.orm.PersistentException;
import orm.*;

/**
 * @version 0.2.0, 10,04,2017
 * @author Eduard QF
 */
public class CreadorBaseRandom {
    
    /**
     * cursos que tendra la institucion, como cantidad por defecto se iniciara con 1.
     */
    int cur=1;
    
    /**
     * asignaturas que tendra la institucion, como cantidad por defecto se iniciara con 1.
     */
    int asig=1;
    
    /**
     * alumnos que tendra cada curso, como cantidad por defecto se iniciara con 1.
     */
    int alum=1;
    
    /**
     * asistencias que tendra cada estudiante, como cantidad por defecto se iniciara con 1.
     */
    int asis=1;
    
    /**
     * actividades que tendra cada asignatura, como cantidad por defecto se iniciara con 1.
     */
    int activ=1;
    
    /**
     *nombre que tendra la institucion;
     */
    private String nombreInst;
     
    /**
     * arreglo que almacena la direccion de la base de datos de los nombres.
     */
    String pathnombre = "C:\\Users\\Eduard QF\\Desktop\\doc\\base nombre.txt";

    /**
     * arreglo que contiene la direccion del archibo que almacena los apellidos.
     */
    String pathApellido = "C:\\Users\\Eduard QF\\Desktop\\doc\\apellido.txt";

    /**
     * arreglo que almacenara los diferentes nombes de una base de datos.
     */
    String nOmbre[];

    /**
     * arreglo que almacenara los diferentes apellidos de una base de datos.
     */
    String aPellido[];

    /**
     *
     * metodo que genera el id de un curso a trabes de la divicion de un numero
     * entero.
     *
     * @param i indica en el numero del arreglo en que va el generador, desde el
     * 0 al 15.
     * @return el id con un digito entre el 1 y el 8 y un caracter alfabetico
     * que puede ser A o B.
     */
    public String newIdcurso(int i) {
        System.out.println(i);
        String id = "";
        int nivel = ((i + 2) / 2);
        if (i % 2 == 0) {
            id += nivel + " A";
        } else {
            id += nivel + " B";
        }
        System.out.println("nombre ingresado curso: " + id);
        return id;
    }

    /**
     * metodo que crea un nuevo curso.
     *
     * @param i indicador de id del curso .
     * @return un curso creado con los datos primarios.
     */
    public Curso newCurso(int i) {
        Curso cur = CursoDAO.createCurso();
        cur.setIdCurso(newIdcurso(i - 1));
        return cur;
    }

    /**
     * metoodo que crea añade una asignatura al curso creado.
     *
     * @param cur curso al que se añadira la asignatura.
     * @param asis asignatura que sera añadida.
     */
    public void addAsign(Curso cur, Asignatura asis) {
        cur.asignatura.add(asis);
    }

    /**
     * metodo que añade un estudiante al curso creado.
     *
     * @param cur curso al que se añadira el estudiante
     * @param alumn estudiante al ser añadido
     */
    public void addaAlumn(Curso cur, Estudiante alumn) {
        cur.estudiante.add(alumn);
    }

    /**
     * metodo que crea una nueva actividad.
     *
     * @param detalle detalle de la nueva actividad a ser creada.
     * @param fecha fecha en que se realizara la actividad.
     * @param plan planificacion a,la que pertenecera la actividad.
     * @return la actividad creada.
     */
    public Actividad newActividad(String detalle, String fecha, Planificacion plan) {
        Actividad act = ActividadDAO.createActividad();
        act.setDetalleAct(detalle);
        act.setFecha(fecha);
        act.setPlanificacion_id_fk(plan);
        return act;
    }

    /**
     * metodo que crea una nueva planificacion de forma basica.
     *
     * @return la planificacion creada.
     */
    public Planificacion newPlanificacion() {
        Planificacion pla = PlanificacionDAO.createPlanificacion();
        return pla;
    }

    /**
     * metodo que añade una actividad a la planificacion.
     *
     * @param plan planificacion a la que se le añadira la actividad.
     * @param actividad actividad a ser añadida.
     */
    public void addActividad(Planificacion plan, Actividad actividad) {
        plan.actividad.add(actividad);

    }

    /**
     * metodo que añade una asignatura a un profesor.
     *
     * @param docent preofesor al que se le añadira la asignatura.
     * @param asign asignatura que sera añadida.
     */
    public void addProfesor(Profesor docent, Asignatura asign) {
        docent.asignatura.add(asign);
    }

    /**
     * metodo que crea una nueva institucion.
     *
     * @param nombre nombre que tendra la institucion.
     * @return la institucion creada con los datos basicos.
     */
    public Institucion newInstitucion(String nombre) {
        Institucion in = InstitucionDAO.createInstitucion();
        in.setNombre(nombre);
        return in;
    }

    /**
     * metodo que añade un curso a la institucion.
     *
     * @param in intitucion ha la que se añadira el curso
     * @param curso curso que sera añadido a la institucion
     */
    public void addCurso(Institucion in, Curso curso) {
        in.curso.add(curso);
    }

    /**
     * metodo que crea un nuevo estudiante.
     *
     * @param per datos personales del estudiante.
     * @return un estudiante creado.
     */
    public Estudiante newAlumnos(Persona per) {
        Estudiante al = EstudianteDAO.createEstudiante();
        al.setPersona_id_fk(per);
        return al;
    }

    /**
     * metodo que crea una nueva anotacion.
     *
     * @param detalle detalle de la anotacion.
     * @param fecha fecha de en que fue colocada la anotacion.
     * @param tipo tipo de anotacion, positiva o negativa.
     * @param estud estudainte al que le pertenece la anotacion.
     * @param prof profesor que coloco la anotacion.
     * @return una notacion creada con todos los datos.
     */
    public Anotaciones newAnotacion(String detalle, String fecha, boolean tipo, Profesor prof, Estudiante estud) {
        Anotaciones an = AnotacionesDAO.createAnotaciones();
        an.setDetalle(detalle);
        an.setFecha(fecha);
        an.setTipo(tipo);
        an.setProfesor_id_fk(prof);
        an.setEstudiante_id_fk(estud);
        return an;
    }

    /**
     * metodo que crea un apoderado.
     *
     * @param per persona que sera apoderado.
     * @return un apoderado con los datos basicos creados.
     */
    public Apoderado newApoderado(Persona per) {
        Apoderado ap = ApoderadoDAO.createApoderado();
        ap.setPersona_id_fk(per);
        return ap;
    }

    /**
     * metodo que crea una nota.
     *
     * @param not nota que obtuvo en la actividad.
     * @param act actividad a la que pertenece la nota.
     * @param es estudiante al que le pertenece la nota.
     * @return una nota creada no todos sus datos.
     */
    public Nota newnota(float not, Actividad act, Estudiante es) {
        Nota nota = NotaDAO.createNota();
        nota.setNota(not);
        nota.setActividad_id_fk1(act);
        nota.setEstudiante_id_fk(es);
        return nota;
    }

    /**
     * metodo que crea una nueva asignatura.
     *
     * @param cur curso al que pertenecera la asignatura.
     * @param nomAsign identificador de la asignatura.
     * @param plan planificacion que tendra la asignatura.
     * @param profe profesor que impartira la asignatura.
     * @return una asignatura creada con todos sus datos.
     */
    public Asignatura newAsignatura(Curso cur, String nomAsign, Planificacion plan, Profesor profe) {
        Asignatura asign = AsignaturaDAO.createAsignatura();
        asign.setCurso_id_fk(cur);
        asign.setNombreAsign(nomAsign);
        asign.setPlanificacion(plan);
        asign.setProfesor_id_fk(profe);
        return asign;
    }

    /**
     * metodo que crea una nueva asistencia.
     *
     * @param b estado de la asistencia.
     * @param fecha fecha de la asistencia.
     * @param estudiante estudiante al que pertenece la asistencias.
     * @return una asistencia con todos sus datos.
     */
    public Asistencia newAsistencia(boolean b, String fecha, Estudiante estudiante) {
        Asistencia asis = AsistenciaDAO.createAsistencia();
        asis.setAsis(b);
        asis.setFecha(fecha);
        asis.setEstudiante_id_fk(estudiante);
        return asis;
    }

    /**
     * metodo que crea un nuevo profesor.
     *
     * @param per persona que sera el docente.
     * @return un profesor con sus datos basicos creados.
     */
    public Profesor newDocentes(Persona per) {
        Profesor pr = ProfesorDAO.createProfesor();
        pr.setPersona_id_fk(per);
        return pr;
    }

    /**
     * metodo que genera un rut alatorio.
     *
     * @return el rut con su digito verificador.
     */
    public static String getRandomRut() {
        int randomNum = new Random().nextInt((21000000 - 10000000) + 1) + 10000000;
        String rut = String.valueOf(randomNum);
        int cantidad = rut.length();
        int factor = 2;
        int suma = 0;
        String verificador = "";
        for (int i = cantidad; i > 0; i--) {
            if (factor > 7) {
                factor = 2;
            }
            suma += (Integer.parseInt(rut.substring((i - 1), i))) * factor;
            factor++;

        }
        verificador = String.valueOf(11 - suma % 11);
        if (verificador.equals("11")) {
            verificador = "0";
        } else if (verificador.equals("10")) {
            verificador = "K";
        }
        return rut + "-" + verificador;
    }

    /**
     * metodo que genera un random entre 0 y 10 y segun este entrega un true o
     * un false.
     *
     * @return on boleano seguin un random que esta entre 0 y 10.
     */
    private  Boolean randomAsis() {
        int n = ThreadLocalRandom.current().nextInt(0, 10);
        if (n < 5) {
            return true;
        }
        return false;
    }

    /**
     * metodo que genera numeros alatorios entre 1 y 7
     *
     * @return un numero alatorio entre 1 y 7 incluyendo decimales.
     */
    private  float randomnot() {
        return (float) (Math.random() * (7 - 1) + 1);
    }

    private Persona newPersona(String nombre, String rut) {
        Persona per = new PersonaDAO().createPersona();
        per.setNombre(nombre);
        per.setRut(rut);
        return per;
    }

    /**
     * metodo que crea un nombre random a base de las listas cargadas.
     */
    private String randomNombre() {
        String ape = aPellido[(int) (Math.random() * aPellido.length)].replaceAll(" ", "");
        String nombreAl = nOmbre[(int) (Math.random() * nOmbre.length)].replaceAll(" ", "") + " " + ape;
        System.out.println("nombre persona:" + nombreAl);
        return nombreAl;
    }

    /**
     * metodo que entrega el nombre de la asignatura correspondiente.
     *
     * @param i indica el id de la asignatura para registrar el nombre.
     * @return la asignatura corrspondiente.
     */
    private String materia(int i) {
        String asig = "";
        switch (i - 1) {
            case 0:
                asig = "Lenguaje";
                break;
            case 1:
                asig = "Matematica";
                break;
            case 2:
                asig = "Ciencias Naturales";
                break;
            case 3:
                asig = "Historia";
                break;
            default:
                asig = "Ingles";
        }
        return asig;
    }

    /**
     * Main aparte de la clace principal del proyecto.
     *
     * @param args the command line arguments
     * @throws org.orm.PersistentException error
     */
    public static void main(String[] args) throws PersistentException {
        CreadorBaseRandom cbr = new CreadorBaseRandom(4,5,2,10,10,"El Tesoro");
        cbr.llenarVectores();
        cbr.llenarBaseDatos();
        System.out.println("iniciando llenado de la base de datos");
    }
   
    
    /**constructor de la clase base random, en la cual indicamos la cantidad de objetos crear de cada uno
     * @param cur cantidad de cursos a crear.
     * @param alum cantidad de alumnos a crear.
     * @param asig cantidad de asignaturas a crear
     * @param asis cantidad de asistencias a crear
     * @param activi cantidad de actividades a crear
     */
    public CreadorBaseRandom(int cur,int alum,int asig,int asis,int activi,String nombreinst){
        this.cur=cur;
        this.alum=alum;
        this.asig=asig;
        this.asis=asis;
        this.activ=activi;
        this.nombreInst=nombreinst;
    }
    
    public void action(){
        try {
            System.out.println("institucion eliminada:"+ capanegocios.Delete.deleteInstitucion());
            llenarVectores();
            llenarBaseDatos();
        } catch (PersistentException ex) {
            Logger.getLogger(CreadorBaseRandom.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * metodo que llena los vectores que contendran los nombres y apellidos.
     */
    public void llenarVectores() {
        nOmbre = DataReader.readData(pathnombre);
        aPellido = DataReader.readData(pathApellido);
        System.out.println("nombre:" + nOmbre[1] + " apellido:" + aPellido[1]);
    }

    /**
     * metodo encargado de generar el poblamiento inicial de la base de datos.
     */
    public void llenarBaseDatos() throws PersistentException {
        //creamos institucion
        Institucion in = newInstitucion(nombreInst);
        boolean de = InstitucionDAO.save(in);
        System.out.println(" colgio creado" + "  guerdado:" + de);
        Institucion loarmIn = InstitucionDAO.loadInstitucionByQuery("id_pk = 1", "id_pk");
        //creamos un ciclo para que cree 16 cursos
        int contp = 0, contd = 0;
        for (int i = 1; i <= cur/*16*/; i++) {
            //creamos curso a añadir
            Curso cur = newCurso(i);
            cur.setInstitucion_id_fk(in);
            System.out.println("curso:" + cur.getIdCurso() + " guardado:" + CursoDAO.save(cur));
            System.out.println("++++++++++++++++++Curso:" + cur.getIdCurso() + "+++++++++++++++++++");
            Curso loarmcur = CursoDAO.loadCursoByQuery("idCurso = '" + newIdcurso(i - 1) + "'", "idCurso");
            for (int j = 1; j <= asig/*6*/; j++) {
                System.out.println("---------------Asignatura:" + j + "---------------");
                //repetir 5 veces
                Planificacion plan = newPlanificacion();
                llenarPlanificacion(plan);

                //creamos la persona
                contp++;
                Persona per = newPersona(randomNombre(), getRandomRut());
                System.out.println("persona" + j + " creada" + "   guerdado:" + PersonaDAO.save(per));
                //creamos profesor para la asignatura
                Profesor profe = newDocentes(per);
                System.out.println("profesor" + j + " creado" + "  guerdado:" + ProfesorDAO.save(profe));
                //creamos la asignatura
                Asignatura asign = newAsignatura(loarmcur, materia(j), plan, profe);

                //añadimos la signatura a la planificacion    
                plan.setAsignatura_id_fk(asign);
                System.out.println("asignatura " + j + " creada" + "  guerdado:" + AsignaturaDAO.save(asign));
                System.out.println("planificacion" + j + " creada  guerdado:" + PlanificacionDAO.save(plan));
                //añadimos la asignatura al curso  
                System.out.println("añadiendo al curso");

                loarmcur.asignatura.add(asign);
                // System.out.println("añadido asignatura " + j + " al curso: " + CursoDAO.save(loarmcur));
            }

            System.out.println("+++++++++++++++++Estudiantes+++++++++++++++");
            for (int j = 1; j <= alum/*30*/; j++) {
                System.out.println("----------------Alumno:" + j + "------------------");
                System.out.println("creando alumno:" + j + " del curso:" + loarmcur.getIdCurso());
                Persona Estud = newPersona(randomNombre(), getRandomRut());
                System.out.println("guardando persona: " + PersonaDAO.save(Estud));
                Persona perAPoder = newPersona(randomNombre(), getRandomRut());
                System.out.println("persona guardada: " + PersonaDAO.save(perAPoder));
                //creamos un estudiante para añadir al curso
                Estudiante alumn = newAlumnos(Estud);
                //creamos un apoderado al alumno
                Apoderado ap = newApoderado(perAPoder);
                //añadimos al apoderado, el alumno correspondiente
                alumn.setApoderado_id_fk(ap);
                alumn.setCurso_id_fk(loarmcur);
                ap.estudiante.add(alumn);
                System.out.println("apoderado " + j + " creado: " + ApoderadoDAO.save(ap));
                //Apoderado loarmAp=ApoderadoDAO.loadApoderadoByQuery("id_pk ="+j, "id_pk");

                //System.out.println("estudiante " + j + " creado: " + EstudianteDAO.save(alumn));
                //Estudiante loarmAlumn = EstudianteDAO.loadEstudianteByQuery("id_pk =" + j*i, "id_pk");
                //añade los demas datos al alumno
                llenarAlumno(alumn);
                //añadimos el estudiante al curso
                System.out.println("relleno del alumno " + j + " guardada:" + EstudianteDAO.save(alumn));
                loarmcur.estudiante.add(alumn);
                System.out.println("loarmcurso:" + loarmcur.getIdCurso());
                System.out.println("estudiante " + j + " creado: " + EstudianteDAO.save(alumn));
            }

            //añadimos el curso a la asignatura
            System.out.println("relleno del curso " + i + " guardado:" + CursoDAO.save(loarmcur));
            loarmIn.curso.add(loarmcur);

        }
        System.out.println("datos actualizados de la Institucion guardados: " + InstitucionDAO.save(loarmIn));
    }

    /**
     * metodo encargado de llenar los demas datos de los estudiantes.
     *
     * @param alumn estudiante al que se le rellenaran los datos.
     */
    private void llenarAlumno(Estudiante alumn) throws PersistentException {
        System.out.print("llenando datos del alumno.....");
        for (int i = 0; i < asis; i++) {
            if (i % 6 == 0) {
                i += 2;
            }
            String fecha = i + "/04/2017";
            //añadimos asistencia
            alumn.asistencia.add(newAsistencia(true, fecha, alumn));
        }

        for (int i = 1; i <= activ/*50*/; i++) {
            Actividad ac = ActividadDAO.loadActividadByQuery("id_pk = " + i, "id_pk");
            Nota not = newnota(randomnot(), ac, alumn);
            alumn.nota.add(not);
            ac.nota1.add(not);
            ActividadDAO.save(ac);
        }
        System.out.println("fin del proseso de llenado.");
    }

    /**
     * metodo encargado de llenar la planificacion con actividades.
     *
     * @param plan planificacion a ser llenada.
     */
    private void llenarPlanificacion(Planificacion plan) {
        System.out.print("llenando planificacion.....");
        for (int i = 1; i <= activ; i++) {
            if (i%2!=0) {
                plan.actividad.add(newActividad("taller "+i, (i*2)+"/03/2017", plan));
            }else{
                plan.actividad.add(newActividad("prueba "+(i-1), (i*2)+"/04/2017", plan));
            }
        }
        System.out.println("fin del proseso de llenado.");
    }
}
