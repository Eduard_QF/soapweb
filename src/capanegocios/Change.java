/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package capanegocios;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.orm.PersistentException;
import org.orm.PersistentTransaction;
import orm.*;

/**
 * @version 0.0.1,23/05/2017
 * @author Eduard QF
 */
public class Change {

    /**
     * metodo que cmabia el nombre de la institucion.
     *
     * @param nombreInstitucion indica el nombre de la institucion.
     * @param newNombre nuevo nombre de la institucion.
     * @return si el cambio se realizao correctamente.
     *
     */
    public static boolean changeInstitucion(String nombreInstitucion, String newNombre) {
        try {
            PersistentTransaction t = orm.LibroClasePersistentManager.instance().getSession().beginTransaction();
            try {
                orm.Institucion loarmInstitucion;
                if (nombreInstitucion != null) {
                    loarmInstitucion = orm.InstitucionDAO.loadInstitucionByQuery("nombre = " + nombreInstitucion, "nombre");
                } else {
                    return false;
                }

                if (newNombre != null) {
                    loarmInstitucion.setNombre(newNombre);
                }
                orm.InstitucionDAO.save(loarmInstitucion);
                t.commit();
                return true;
            } catch (PersistentException e) {
                t.rollback();
                return false;
            }

        } catch (PersistentException ex) {
            Logger.getLogger(Change.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;

    }

    /**
     * metodo que modifica los datos de un curso.
     *
     * @param idCurso indica el indicador del curso que es.
     * @param institucion indica la institucin a la que pertenece el curso.
     * @param newinstitucion indica la nueva institucion perteneciente.
     * @param newidCurso indica el nuevo id del curso
     * @return si el cambio se realizao correctamente.
     *
     */
    public static boolean changeCurso(String idCurso, orm.Institucion institucion, orm.Institucion newinstitucion, String newidCurso) {
        try {
            PersistentTransaction t = orm.LibroClasePersistentManager.instance().getSession().beginTransaction();
            try {
                orm.Curso loarmCurso = orm.CursoDAO.loadCursoByQuery("idCurso = " + idCurso + "' AND ' institucion_id_fk = " + institucion.getNombre() + "'", "idCurso");
                if (newidCurso != null) {
                    loarmCurso.setIdCurso(newidCurso);
                }
                if (!newinstitucion.getNombre().equals(institucion.getNombre())) {
                    loarmCurso.setInstitucion_id_fk(newinstitucion);
                }
                orm.CursoDAO.save(loarmCurso);
                t.commit();
                return true;
            } catch (PersistentException e) {
                t.rollback();
                return false;
            }

        } catch (PersistentException ex) {
            Logger.getLogger(Change.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;

    }

    /**
     * metodo que modifica los datos de una asignatura.
     *
     * @param nombre indica el nombre de la asingatura.
     * @param cur indica el curso al que pertenese la asingatura.
     * @param newNombre nuevo nombre a tener la asignatura.
     * @param newPlan nueva planificacion a tener la asignatura.
     * @param newDocente nuevo docente perteneciente a la planificacion.
     * @param newCurso indica el nuevo curso al que peretenesera la asignatura.
     * @return si el cambio se realizao correctamente.
     *
     */
    public static boolean changeAsignatura(String nombre, orm.Curso cur, String newNombre, orm.Planificacion newPlan, orm.Profesor newDocente, orm.Curso newCurso) {
        try {
            PersistentTransaction t = orm.LibroClasePersistentManager.instance().getSession().beginTransaction();
            try {
                orm.Asignatura loarmAsignatura = orm.AsignaturaDAO.loadAsignaturaByQuery("'nombreAsign = " + nombre + "' AND ' curso_id_fk = " + cur.getId(), "curso_id_fk");
                if (!newCurso.getIdCurso().equals(cur.getIdCurso())) {
                    loarmAsignatura.setCurso_id_fk(newCurso);
                }
                if (newNombre != null) {
                    loarmAsignatura.setNombreAsign(newNombre);
                }
                if (newPlan.getId() != loarmAsignatura.getPlanificacion().getId()) {
                    loarmAsignatura.setPlanificacion(newPlan);
                }
                if (!newDocente.getPersona_id_fk().getRut().equals(loarmAsignatura.getProfesor_id_fk().getPersona_id_fk().getRut())) {
                    loarmAsignatura.setProfesor_id_fk(newDocente);
                }
                orm.AsignaturaDAO.save(loarmAsignatura);
                t.commit();
                return true;
            } catch (PersistentException e) {
                t.rollback();
                return false;
            }

        } catch (PersistentException ex) {
            Logger.getLogger(Change.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;

    }

    /**
     * metodo que modifica los datos de un estudiante.
     *
     * @param nombre indica el nombre actual del estudiante.
     * @param rut indica el rut actual del estudiante.
     * @param newApoderado entrega el nuevo apoderado.
     * @param newCurso entrega el nuevo curso al que pertenecera.
     * @param newNombre entrega el nuevo nombre que tendra.
     * @param newRut entrega el nuevo rut que tendra.
     * @param newFinalAsis entrega la nueva asistencia final
     * @return si el cambio se realizao correctamente.
     *
     */
    public static boolean changeEstudiante(String nombre, String rut, orm.Apoderado newApoderado, orm.Curso newCurso, String newNombre, String newRut, int newFinalAsis) {
        try {
            PersistentTransaction t = orm.LibroClasePersistentManager.instance().getSession().beginTransaction();
            try {
                orm.Persona per = orm.PersonaDAO.loadPersonaByQuery("rut = " + rut, "rut");
                orm.Estudiante loarmEstudiante = orm.EstudianteDAO.loadEstudianteByQuery("persona_id_fk =" + per.getId(), "id_pk");
                if (newApoderado != null && newApoderado.getPersona_id_fk().getNombre().equals(loarmEstudiante.getApoderado_id_fk().getPersona_id_fk().getNombre())) {
                    loarmEstudiante.setApoderado_id_fk(newApoderado);
                }
                if (newCurso != null && !newCurso.getIdCurso().equals(loarmEstudiante.getCurso_id_fk().getIdCurso())) {
                    loarmEstudiante.setCurso_id_fk(newCurso);
                }
                if (newRut != null) {
                    loarmEstudiante.getPersona_id_fk().setRut(newRut);
                }
                if (newNombre != null) {
                    loarmEstudiante.getPersona_id_fk().setRut(newRut);
                }
                if (newFinalAsis >= 0 && newFinalAsis <= 100) {
                    loarmEstudiante.setAsistenciaFinal(newFinalAsis);
                }
                orm.EstudianteDAO.save(loarmEstudiante);
                t.commit();
                return true;
            } catch (PersistentException e) {
                t.rollback();
                return false;
            }

        } catch (PersistentException ex) {
            Logger.getLogger(Change.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;

    }

    /**
     * metodo que modifica los datos de un apoderado.
     *
     * @param nombre entrega el nombre actual del apoderado.
     * @param rut entrega el rut actual del apoderado.
     * @param newNombre entrega el nuevo nombre del apoderado.
     * @param newRut entrega el nuevo rut del apoderado.
     * @return si el cambio se realizao correctamente.
     *
     */
    public static boolean changeApoderado(String nombre, String rut, String newNombre, String newRut) {
        try {
            PersistentTransaction t = orm.LibroClasePersistentManager.instance().getSession().beginTransaction();
            try {
                orm.Persona per = orm.PersonaDAO.loadPersonaByQuery("rut = '" + rut + "'", "rut");
                orm.Apoderado loarmApoderado = orm.ApoderadoDAO.loadApoderadoByQuery("persona_id_fk = " + per.getId(), "persona_id_fk");
                if (newNombre != null) {
                    loarmApoderado.getPersona_id_fk().setNombre(newNombre);
                }
                if (newRut != null) {
                    loarmApoderado.getPersona_id_fk().setRut(newRut);
                }
                t.commit();
                return true;
            } catch (PersistentException e) {
                t.rollback();
                System.out.println(e);
                return false;
            }

        } catch (PersistentException ex) {
            Logger.getLogger(Change.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;

    }

    /**
     * metodo que modifica los datos de un apoderado.
     *
     * @param rut entrega el rut actual del apoderado.
     * @param addEstudiante estudiante por el que se generara el cambio.
     * @param removeEstudiante estudiante que sera eliminado o cambiado
     * @return si el cambio se realizao correctamente.
     *
     */
    public static boolean changeEstdiantes_Apoderado(String rut, Estudiante addEstudiante, Estudiante removeEstudiante) {
        try {
            PersistentTransaction t = orm.LibroClasePersistentManager.instance().getSession().beginTransaction();
            try {
                orm.Persona per = orm.PersonaDAO.loadPersonaByQuery("rut = '" + rut + "'", "rut");
                orm.Apoderado loarmApoderado = orm.ApoderadoDAO.loadApoderadoByQuery("persona_id_fk = " + per.getId(), "persona_id_fk");
                if (addEstudiante != null) {
                    loarmApoderado.estudiante.add(addEstudiante);
                }
                if (removeEstudiante != null) {
                    loarmApoderado.estudiante.remove(removeEstudiante);
                }
                t.commit();
                return true;
            } catch (PersistentException e) {
                t.rollback();
                System.out.println(e);
                return false;
            }

        } catch (PersistentException ex) {
            Logger.getLogger(Change.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;

    }

    /**
     * modifica los datos de un profesor.
     *
     * @param nombre entrega el nombre actual del profesor.
     * @param rut entrega el rut del profesir actual.
     * @param newRut entrega el nuevo rut.
     * @param newNombre entrega el nuevo nombre.
     * @return si el cambio se realizao correctamente.
     *
     */
    public static boolean changeProfesor(String nombre, String rut, String newRut, String newNombre) {
        try {
            PersistentTransaction t = orm.LibroClasePersistentManager.instance().getSession().beginTransaction();
            try {
                orm.Persona per = orm.PersonaDAO.loadPersonaByQuery("rut = " + rut, "rut");
                orm.Profesor loarmProfesor = orm.ProfesorDAO.loadProfesorByQuery("persona_id_fk = " + per.getId(), "per_id_fk");
                if (newNombre != null) {
                    loarmProfesor.getPersona_id_fk().setNombre(newNombre);
                }
                if (newRut != null) {
                    loarmProfesor.getPersona_id_fk().setRut(newRut);
                }
                orm.ProfesorDAO.save(loarmProfesor);
                t.commit();
                return true;
            } catch (PersistentException e) {
                t.rollback();
                return false;
            }

        } catch (PersistentException ex) {
            Logger.getLogger(Change.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;

    }

    /**
     * metodo que modifica los datos de una planificaion.
     *
     * @param asign entrega la asignatura a la que pertenece la planificacion.
     * @param newAsign entrega la nueva asignatura a la que pertenecera.
     * @return si el cambio se realizao correctamente.
     *
     */
    public static boolean changePlanificacion(orm.Asignatura asign, orm.Asignatura newAsign) {
        try {
            PersistentTransaction t = orm.LibroClasePersistentManager.instance().getSession().beginTransaction();
            try {
                orm.Planificacion loarmPlanificacion = orm.PlanificacionDAO.loadPlanificacionByQuery("asignatura_id_fk = " + asign.getId_(), "id_pk");
                if (newAsign != null && (newAsign.getId_() == loarmPlanificacion.getAsignatura_id_fk().getId_())) {
                    loarmPlanificacion.setAsignatura_id_fk(newAsign);
                }
                orm.PlanificacionDAO.save(loarmPlanificacion);
                t.commit();
                return true;
            } catch (PersistentException e) {
                t.rollback();
                return false;
            }

        } catch (PersistentException ex) {
            Logger.getLogger(Change.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;

    }

    /**
     * metodo que modifica los datos de una actividad.
     *
     * @param fecha fecha actual de la actividad.
     * @param plan planificacion a la que pertenece.
     * @param newFecha nueva fecha de la actividad.
     * @param newDetalle nuevo detalle de la actividad.
     * @param newPlan nueva planificacion a la pertenece la actividad.
     * @return si el cambio se realizao correctamente.
     *
     */
    public static boolean changeActividad(String fecha, orm.Planificacion plan, String newFecha, String newDetalle, orm.Planificacion newPlan) {
        try {
            PersistentTransaction t = orm.LibroClasePersistentManager.instance().getSession().beginTransaction();
            try {
                orm.Actividad loarmActividad = orm.ActividadDAO.loadActividadByQuery("fecha = " + fecha + " ' AND ' planificaion_id_fk = " + plan.getId(), "");
                if (newDetalle != null) {
                    loarmActividad.setDetalleAct(newDetalle);
                }

                if (newFecha != null) {
                    loarmActividad.setFecha(newFecha);
                }

                if (newPlan != null) {
                    loarmActividad.setPlanificacion_id_fk(newPlan);
                }
                orm.ActividadDAO.save(loarmActividad);
                t.commit();
                return true;
            } catch (PersistentException e) {
                t.rollback();
                return false;
            }

        } catch (PersistentException ex) {
            Logger.getLogger(Change.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;

    }

    /**
     * metodo que modifica los datos de una asistencia.
     *
     * @param fecha fecha actual de la asistencia.
     * @param alumno estudiante al que pertenece la asistencia.
     * @param newFecha nueva fecha en que se paso la asistencia.
     * @param newAsis cambia el estado de la asistencia.
     * @return si el cambio se realizao correctamente.
     *
     */
    public static boolean changeAsistencia(String fecha, orm.Estudiante alumno, String newFecha, boolean newAsis) {
        try {
            PersistentTransaction t = orm.LibroClasePersistentManager.instance().getSession().beginTransaction();
            try {
                orm.Asistencia loarmAsistencia = orm.AsistenciaDAO.loadAsistenciaByQuery("fecha =" + fecha + "' And ' estudiante_id_fk =" + alumno.getId(), "estudiante_id_fk");
                if (newAsis != loarmAsistencia.getAsis()) {
                    loarmAsistencia.setAsis(newAsis);
                }
                if (newFecha != null) {
                    loarmAsistencia.setFecha(newFecha);
                }

                t.commit();
                return true;
            } catch (PersistentException e) {
                t.rollback();
                return false;
            }

        } catch (PersistentException ex) {
            Logger.getLogger(Change.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;

    }

    /**
     * metodo que modifica los datos de una Anotacion.
     *
     * @param fecha fecha actual de la anotacion.
     * @param alumno alumno al que pertenece el alumno.
     * @param newDetalle nuevo detalle de la anotacion.
     * @param newFecha nueva fecha de la anotacion.
     * @param newTipo cambia el estado de la anotacion.
     * @param newdocente docente que se le añadira a la anotacion.
     * @return si el cambio se realizao correctamente.
     *
     */
    public static boolean changeAnotacion(String fecha, orm.Estudiante alumno, String newDetalle, String newFecha, boolean newTipo, orm.Profesor newdocente) {
        try {
            PersistentTransaction t = orm.LibroClasePersistentManager.instance().getSession().beginTransaction();
            try {
                orm.Anotaciones loarmAnotacion = orm.AnotacionesDAO.loadAnotacionesByQuery("estudiante_id_fk = " + alumno.getId() + "' And 'fecha" + fecha + "'", fecha);
                if (newDetalle != null) {
                    loarmAnotacion.setDetalle(newDetalle);
                }
                if (newFecha != null) {
                    loarmAnotacion.setFecha(newFecha);
                }
                if (newTipo != loarmAnotacion.getTipo()) {
                    loarmAnotacion.setTipo(newTipo);
                }

                if (newdocente != null) {
                    loarmAnotacion.setProfesor_id_fk(newdocente);
                }
                orm.AnotacionesDAO.save(loarmAnotacion);
                t.commit();
                return true;
            } catch (PersistentException e) {
                t.rollback();
                return false;
            }

        } catch (PersistentException ex) {
            Logger.getLogger(Change.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;

    }

    /**
     * metodo que modifica una nota.
     *
     * @param act actividad a la que pertenece la nota.
     * @param alumno estudiante al que pertenece la nota.
     * @param nota nueva calificacion a tener la nota.
     * @return si el cambio se realizao correctamente.
     *
     */
    public static boolean changeNota(orm.Actividad act, orm.Estudiante alumno, float nota) {
        try {
            PersistentTransaction t = orm.LibroClasePersistentManager.instance().getSession().beginTransaction();
            try {
                orm.Nota loarmNota = orm.NotaDAO.loadNotaByQuery("estudiante_id_fk" + alumno.getId() + "' AND 'actividad_id_fk1" + act.getPlanificacion_id_fk() + "'", "estudiante_id_fk");
                if (nota >= 1.0 && nota <= 7.0) {
                    loarmNota.setNota(nota);
                }
                orm.NotaDAO.save(loarmNota);
                t.commit();
                return true;
            } catch (PersistentException e) {
                t.rollback();
                return false;
            }

        } catch (PersistentException ex) {
            Logger.getLogger(Change.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;

    }

    /**
     * metodo que modifica los datos de una persona.
     *
     * @param nombre nombre actual de la persona.
     * @param rut rut actual de la persona.
     * @param newNombre nuevo nombre de la persona.
     * @param newRut nuevo rut de la persona.
     * @return si el cambio se realizao correctamente.
     *
     */
    public static boolean changePersona(String nombre, String rut, String newNombre, String newRut) {
        try {
            PersistentTransaction t = orm.LibroClasePersistentManager.instance().getSession().beginTransaction();
            try {
                orm.Persona loarmPersona = orm.PersonaDAO.loadPersonaByQuery("nombre =" + nombre + "' AND ' rut =" + rut + "'", "rut");
                if (newNombre != null) {
                    loarmPersona.setNombre(nombre);
                }
                if (newRut != null) {
                    loarmPersona.setRut(rut);
                }

                orm.PersonaDAO.save(loarmPersona);
                t.commit();
                return true;
            } catch (PersistentException e) {
                t.rollback();
                return false;
            }
        } catch (PersistentException ex) {
            Logger.getLogger(Change.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
}
