/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Planificacion {
	public Planificacion() {
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == orm.ORMConstants.KEY_PLANIFICACION_ACTIVIDAD) {
			return ORM_actividad;
		}
		
		return null;
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
	};
	
	private orm.Asignatura asignatura_id_fk;
	
	private int id;
	
	private java.util.Set ORM_actividad = new java.util.HashSet();
	
	private void setId(int value) {
		this.id = value;
	}
	
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	public void setAsignatura_id_fk(orm.Asignatura value) {
		if (this.asignatura_id_fk != value) {
			orm.Asignatura lasignatura_id_fk = this.asignatura_id_fk;
			this.asignatura_id_fk = value;
			if (value != null) {
				asignatura_id_fk.setPlanificacion(this);
			}
			if (lasignatura_id_fk != null && lasignatura_id_fk.getPlanificacion() == this) {
				lasignatura_id_fk.setPlanificacion(null);
			}
		}
	}
	
	public orm.Asignatura getAsignatura_id_fk() {
		return asignatura_id_fk;
	}
	
	private void setORM_Actividad(java.util.Set value) {
		this.ORM_actividad = value;
	}
	
	private java.util.Set getORM_Actividad() {
		return ORM_actividad;
	}
	
	public final orm.ActividadSetCollection actividad = new orm.ActividadSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_PLANIFICACION_ACTIVIDAD, orm.ORMConstants.KEY_ACTIVIDAD_PLANIFICACION_ID_FK, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
