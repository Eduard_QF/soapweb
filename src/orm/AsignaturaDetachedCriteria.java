/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class AsignaturaDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id_;
	public final IntegerExpression curso_id_fkId;
	public final AssociationExpression curso_id_fk;
	public final IntegerExpression profesor_id_fkId;
	public final AssociationExpression profesor_id_fk;
	public final StringExpression nombreAsign;
	public final IntegerExpression planificacionId;
	public final AssociationExpression planificacion;
	
	public AsignaturaDetachedCriteria() {
		super(orm.Asignatura.class, orm.AsignaturaCriteria.class);
		id_ = new IntegerExpression("id_", this.getDetachedCriteria());
		curso_id_fkId = new IntegerExpression("curso_id_fk.id", this.getDetachedCriteria());
		curso_id_fk = new AssociationExpression("curso_id_fk", this.getDetachedCriteria());
		profesor_id_fkId = new IntegerExpression("profesor_id_fk.id", this.getDetachedCriteria());
		profesor_id_fk = new AssociationExpression("profesor_id_fk", this.getDetachedCriteria());
		nombreAsign = new StringExpression("nombreAsign", this.getDetachedCriteria());
		planificacionId = new IntegerExpression("planificacion.id_", this.getDetachedCriteria());
		planificacion = new AssociationExpression("planificacion", this.getDetachedCriteria());
	}
	
	public AsignaturaDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, orm.AsignaturaCriteria.class);
		id_ = new IntegerExpression("id_", this.getDetachedCriteria());
		curso_id_fkId = new IntegerExpression("curso_id_fk.id", this.getDetachedCriteria());
		curso_id_fk = new AssociationExpression("curso_id_fk", this.getDetachedCriteria());
		profesor_id_fkId = new IntegerExpression("profesor_id_fk.id", this.getDetachedCriteria());
		profesor_id_fk = new AssociationExpression("profesor_id_fk", this.getDetachedCriteria());
		nombreAsign = new StringExpression("nombreAsign", this.getDetachedCriteria());
		planificacionId = new IntegerExpression("planificacion.id_", this.getDetachedCriteria());
		planificacion = new AssociationExpression("planificacion", this.getDetachedCriteria());
	}
	
	public CursoDetachedCriteria createCurso_id_fkCriteria() {
		return new CursoDetachedCriteria(createCriteria("curso_id_fk"));
	}
	
	public ProfesorDetachedCriteria createProfesor_id_fkCriteria() {
		return new ProfesorDetachedCriteria(createCriteria("profesor_id_fk"));
	}
	
	public PlanificacionDetachedCriteria createPlanificacionCriteria() {
		return new PlanificacionDetachedCriteria(createCriteria("planificacion"));
	}
	
	public Asignatura uniqueAsignatura(PersistentSession session) {
		return (Asignatura) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Asignatura[] listAsignatura(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Asignatura[]) list.toArray(new Asignatura[list.size()]);
	}
}

