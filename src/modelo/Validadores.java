/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import orm.Estudiante;
import orm.Nota;

/**
 * @version 0.0.0,28/04/2017.
 * @author Eduard QF
 */
public class Validadores {

    /**
     * metodo que valida los digitos de un rut
     *
     * @param cadena String a validar
     *
     * @return devuelve true en caso de coindidir
     */
    public static boolean validarRut(String cadena) {
        cadena = cadena.toUpperCase();
        if (validarFormatoRut(cadena)) {
            String n = cadena.substring(0, cadena.length() - 2);
            String d = cadena.substring(cadena.length() - 1);
            n = n.replaceAll("\\.", "");
            int v = digitoVerificador(n);
            if (v < 10 && d.equals(String.valueOf(v))) {
                return true;
            } else if (v == 10 && d.equals("K")) {
                return true;
            }
        }
        return false;
    }

    /**
     * metodo que valida el formato de un rut
     *
     * @param r String a validar
     *
     * @return devuelve true en caso de coindidir
     */
    public static boolean validarFormatoRut(String r) {
        return r.matches("[0-9]{1,2}\\.{0,1}[0-9]{3,3}\\.{0,1}[0-9]{3,3}\\-[0-9K]{1,1}");
    }

    /**
     * metodo que valida el digito verificador de un rut
     *
     * @param r String a validar
     *
     * @return devuelve el numero del verificador
     */
    private static int digitoVerificador(String r) {
        int v = 0;
        int c = 1;
        int s = 0;
        int d[] = new int[r.length()];
        for (int i = r.length() - 1; i >= 0; i--) {
            d[i] = Integer.valueOf(r.substring(i, i + 1));
            c++;
            if (c > 7) {
                c = 2;
            }
            s += (d[i] * c);
        }
        v = 11 - (s % 11);
        if (v == 11) {
            v = 0;
        }
        return v;
    }

    /**
     * metodo que valida el formato de numeros positivos.
     *
     * @param text texto que se le verificara el formato de numero
     * @return true al coincidir el formato
     */
    public static boolean vFormatnumPos(String text) {
        return text.matches("[0-9]+");
    }

    /**
     * metodo que obtiene el mayor de dos numeros.
     *
     * @param porsientoAsistencia asistencia obtenida por el alumno.
     * @param pAsis asistencia requerida.
     * @return true al ser porsieto de asistencia menor que la asistencia
     * requerida.
     */
    public static boolean porsentaje(float porsientoAsistencia, int pAsis) {
        return porsientoAsistencia < pAsis;
    }

    /**
     * metodo qu valida el formato de un nombre.
     *
     * @param text texto a comprobar el formato del nombre.
     * @return true si el formato es correcto.
     */
    public static boolean validarFormatoNombre(String text) {
        if (text.length() >= 4) {
            if (!text.contains("[0-9]")) {
                return text.matches("[a-zA-Z\\s]+");
            }
        }
        return false;
    }

    /**
     * metodo que valida a los alumnos reprobados
     *
     * @param alumn estudiante al que se comprobara si esta reprobado.
     * @return true si paso todas las pruebas de tener hacistencia mayor al 85%
     * y promediio de notas mayores al 4.
     */
    public static boolean reprobado(Estudiante alumn) {
        int contR = 0;
        float promedio = 0;
        String materia = null;
        if ((alumn.nota.toArray().length) == 0) {
            return false;
        } else {
            for (Nota ev : alumn.nota.toArray()) {
                if (materia == null) {
                    materia = ev.getActividad_id_fk1().getPlanificacion_id_fk().getAsignatura_id_fk().getNombreAsign();
                }
                if (ev.getActividad_id_fk1().getPlanificacion_id_fk().getAsignatura_id_fk().getNombreAsign().equals(materia)) {
                    promedio += (ev.getNota() / alumn.nota.toArray().length);
                } else {
                    materia = ev.getActividad_id_fk1().getPlanificacion_id_fk().getAsignatura_id_fk().getNombreAsign();
                    if (promedio < 4.0) {
                        contR++;
                    }//0.2+0.4+0.6+0.8+1
                    promedio = 0;
                }
            }
        }
        return !(alumn.getAsistenciaFinal() < 85 || contR >= 3);
    }

    /**
     * metodo que valida el formato de un curso
     *
     * @param idCurso nombre que recivira el curso.
     * @return true en al caso de que el formato sea correcto
     */
    public static boolean validarFormatCurso(String idCurso) {

        return idCurso.matches("[1-8]{1,1} [A-Z]{1,1}");
    }
}
