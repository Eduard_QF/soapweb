package modelo;


import com.google.gson.Gson;
import com.thoughtworks.xstream.XStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import org.orm.PersistentException;
import orm.*;

/**
 * @version 0,4,0 20/04/2017
 * @author Eduard QF
 */
public class Body {

    /**
     * atributo que almacena la instancia de INstitucion.
     */
    private Institucion inst;

    /**
     * metodo que inicia el objeto Body pero no es el constructor de esta clase.
     * @throws org.orm.PersistentException error
     * @deprecated 
     * @throws java.io.FileNotFoundException error
     */
    public void init() throws FileNotFoundException, PersistentException {
//        XStream xs = new XStream();
//        Gson gson = new Gson();
//        String DatosTotal = datos.DataReader.readDataS("C:\\Users\\Eduard QF\\Documents\\NetBeansProjects\\ProgramacionCursosBasica\\src\\datos\\DatosTotal.json");
//        inst = gson.fromJson(DatosTotal, Institucion.class);
//        inst = (Institucion) xs.fromXML(new FileReader("C:\\Users\\Eduard QF\\Documents\\NetBeansProjects\\ProgramacionCursosBasica\\src\\datos\\DatosTotal.xml"));
        inst=InstitucionDAO.loadInstitucionByQuery("id_pk = 1", "id_pk");
    }

    /**
     * metodo que entrega la istanciacion de Instituto para generar una
     * coceccion con las ventanas.
     *
     * @return la instanciacion de Instituto
     */
    public Institucion asignInst() {
        return inst;
    }

    /**
     * metodo que compara los datos entregados con los de los Docente para
     * retornar cual de todos es.
     *
     * @param profesores vector que contiene todos los profesores del curso.
     * @param nombre nombre del profesor que se busca.
     * @deprecated 
     * @return el Profesor correspondiente a los datos entregados en los
     * parametros.
     */
    public Profesor compareDocentes(Profesor[] profesores, String nombre) {
        for (Profesor profesor:profesores) {
            if ((profesor.getPersona_id_fk().getNombre()).equals(nombre)) {
                return profesor;
            }
        }
        return null;
    }


    /**
     * metodo encagado de guardar los cambios generados.
     * @throws java.io.FileNotFoundException error
     */
    public void guardarCambios()throws FileNotFoundException {
        XStream xs = new XStream();
        xs.alias("Institucion", Institucion.class);
        String t = xs.toXML(inst);
        datos.DataReader.writeData("C:\\InstitucionBasica\\documents\\DatosTotal.xml", t);
        System.out.println("xml creado");
        Gson g = new Gson();
        String p = g.toJson(inst);
        datos.DataReader.writeData("C:\\InstitucionBasica\\documents\\DatosTotal.xml", p);
        System.out.println("json creado");
    }

    /**
     * metodo encargado de obtener la fecha que tiene estipulada el computador
     * en el que se trabaja.
     *
     * @return la fecha en la que nos encontrames
     */
    public String getFecha() {
        Date fecha = new Date();
        SimpleDateFormat formateador = new SimpleDateFormat("dd-MM-yyyy");
        return formateador.format(fecha);
    }

    /**
     * metodo que compara dos cadenas de strin por medio del metodo equals.
     *
     * @param text cadena uno
     * @param string cadena 2
     * @return el resultado de la comparacion, true o false.
     */
    public boolean comparadorString(String text, String string) {
        return text.equals(string);
    }

    /**
     * metodo que indica el Id del curso a traves de un index entregado.
     *
     * @param i index del cual se obtendra el Id del curso.
     * @return un curso que varia entre los niveles 1 y 8 y los rangos A y B por
     * cada uno.
     */
    public String getIdcurso(int i) {
        String id = "";
        int nivel = ((i + 2) / 2);
        if (i % 2 == 0) {
            id += nivel + " A";
        } else {
            id += nivel + " B";
        }
        System.out.println("rellenando curso: " + id);
        return id;
    }

    /**
     * metodo que obtiene la asignatura correspondiente a un index entregado.
     *
     * @param i index del cual se obtendra la asignatura perteneciente.
     * @return el nombre de la asignatura correspondiente al index i.
     */
    public String getAsignatura(int i) {
        String asig = "";
        switch (i) {
            case 0:
                asig = "Lenguaje";
                break;
            case 1:
                asig = "Matematica";
                break;
            case 2:
                asig = "Ciencias Naturales";
                break;
            case 3:
                asig = "Historia";
                break;
            default:
                asig = "Ingles";
        }
        return asig;
    }

    /**
     * metodo que compara el nombre de la asignatura con un texto entregado.
     *
     * @param materia indica el texto entregado a comparar con la asignatura.
     * @param asign nombre de la asignatura.
     * @return true si las dos cadenas coinciden y false si no lo hacen.
     */
    public boolean asignatura(String materia, String asign) {
        return materia.equals(asign);
    }

    /**
     * metodo que llena el jcombobox que contienen los cursos
     * @return metodo que entega todos los  ids de los cursos
     */
    public static String [] llenarCurso()  {
        orm.Curso[] curso = capanegocios.Read.readCursos();
        String []idCursos = new String[curso.length];
        for (int i = 0; i < curso.length; i++) {
            System.out.println("Curso:"+ curso[i].getIdCurso());
            idCursos[i] = curso[i].getIdCurso();
        }
        Arrays.sort(idCursos);
        return idCursos;
    }
    
    public static String[] llenaraApoderados(){
        Apoderado ap[]=capanegocios.Read.readApoderadosInstitucion();
        String []apoderados=new String[ap.length];
        for (int i=0;i<ap.length;i++) {
            apoderados[i]=ap[i].getPersona_id_fk().getNombre();
        }
        return apoderados;
    }
    
    /**
     * metodo encargado de ordenar los arreglos.
     * @param vectores arreglo a ser ordenado.
     * @return el mismo vector pero ordenado de forma asendente.
     * @throws org.orm.PersistentException error
     */
    public String [] ArraysOrder(String[] vectores) throws PersistentException {
        Arrays.sort(vectores);
        return vectores;
    }

    /**
     * metodo que compara las actividades con el titulo de uno.
     * @param actividades arreglo de las actividades presente en la asignatura.
     * @param activ titulo de la actividad que se busca.
     * @return la actividad que se busca, si es que se encuentra.
     */
    public Actividad compareActiv(Actividad[] actividades, String activ) {
        for(Actividad ac:actividades){
            if (ac.getDetalleAct().equals(activ)) {
                return ac;
            }
        }
        return null;
    }

    /**
     * metodo que compara los id de los cursos exitentes con un nuevo id.
     * @param idCurso identificador del curso que se busca.
     * @return true si el identificador no se encuentra y false si ya a sido encontrado 
     */
    public boolean compareCurso(String idCurso)  {
        for (Curso cur:capanegocios.Read.readCursos()) {
            if (cur.getIdCurso().equals(idCurso)) {
                return false;
            }
        }
        return true;
    }

    public static String[] llenardocent()  {
        orm.Profesor[] prof=capanegocios.Read.readProfesoresInstitucion();
        String[] nombres=new String[prof.length];
        for (int i=0;i<prof.length;i++) {
            System.out.println("Dcente :"+prof[i].getPersona_id_fk().getNombre());
            nombres[i]=(prof[i].getPersona_id_fk().getNombre());
            
        }
        return nombres;
    }
    

}
